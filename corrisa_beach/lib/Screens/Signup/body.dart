import 'package:corrisa_beach/main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:corrisa_beach/Screens/Login/login_screen.dart';
import 'package:corrisa_beach/Screens/Signup/background.dart';
import 'package:corrisa_beach/components/or_divider.dart';
import 'package:corrisa_beach/components/social_icon.dart';
import 'package:corrisa_beach/components/already_have_an_account_acheck.dart';
import 'package:corrisa_beach/components/rounded_button.dart';
import 'package:corrisa_beach/components/rounded_input_field.dart';
import 'package:corrisa_beach/components/rounded_password_field.dart';

class Body extends StatefulWidget {
  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final emailController = TextEditingController();
  final passController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.03),
            Image(
              image: AssetImage("assets/images/x4.png"),
              height: size.height * 0.3,
            ),
            // SvgPicture.asset(
            //   "assets/icons/signup.svg",
            //   height: size.height * 0.35,
            // ),
            RoundedInputField(
              color: Color(0xFF6F35A5),
              color2: Color(0xFFF1E6FF),
              hintText: "Email",
              controller: emailController,
            ),
            RoundedPasswordField(
              color: Color(0xFF6F35A5),
              color2: Color(0xFFF1E6FF),
              controller: passController,
            ),
            RoundedButton(
              color: Color(0xFF6F35A5),
              text: "REGISTER",
              press: () {
                register();
              },
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              color: Color(0xFF6F35A5),
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            OrDivider(
              color: Color(0xFF6F35A5),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocalIcon(
                  color: Color(0xFFF1E6FF),
                  iconSrc: "assets/icons/facebook.svg",
                  press: () {},
                ),
                SocalIcon(
                  color: Color(0xFFF1E6FF),
                  iconSrc: "assets/icons/google.svg",
                  press: () {},
                ),
                SocalIcon(
                  color: Color(0xFFF1E6FF),
                  iconSrc: "assets/icons/twitter2.svg",
                  press: () {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future register() async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => Center(
              child: CircularProgressIndicator(),
            ));

    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: emailController.text.trim(),
          password: passController.text.trim());
    } on FirebaseAuthException catch (e) {
      await showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) => AlertDialog(
                title: Text(
                  "Pengumuman",
                  style: TextStyle(color: Color(0xFF6F35A5)),
                ),
                content: Text(e.message!),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        "OK",
                        style: TextStyle(color: Color(0xFF6F35A5)),
                      ))
                ],
              ));
      print(e);
    }

    navigatorKey.currentState!.popUntil((route) => route.isFirst);
  }
}
