import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/4.png"),
              alignment: Alignment.bottomCenter),
          gradient: LinearGradient(colors: [
            Colors.orange.withOpacity(0.3),
            Colors.orange.withOpacity(0.015),
            Colors.orange.withOpacity(0.3),
          ])),
      child: Stack(
        // alignment: Alignment.center,

        children: <Widget>[
          child, // Ini child untuk body.dart
        ],
      ),
    );
  }
}
