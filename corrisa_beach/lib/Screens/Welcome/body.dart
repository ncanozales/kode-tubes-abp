import 'package:flutter/material.dart';
import 'package:corrisa_beach/Screens/Login/login_screen.dart';
import 'package:corrisa_beach/Screens/Welcome/background.dart';
import 'package:corrisa_beach/components/rounded_button.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(
      child: SingleChildScrollView(
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.08),
              Text(
                "Welcome to",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.orange.withOpacity(.5),
                    fontFamily: 'lima',
                    fontSize: 40),
              ),
              Text(
                "Corissa Beach",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.orange.withOpacity(.5),
                    fontFamily: 'lima',
                    fontSize: 40),
              ),
              SizedBox(height: size.height * 0.5),
              // SvgPicture.asset(
              //   "assets/icons/chat.svg",
              //   height: size.height * 0.45,
              // ),
              SizedBox(height: size.height * 0.13),
              RoundedButton(
                color: Colors.yellow.withOpacity(.3),
                textColor: Colors.white,
                text: "Enter",
                press: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  )
                },
              ),
              // RoundedButton(
              //   text: "Register",
              //   color: Colors.yellow.withOpacity(.3),
              //   textColor: Colors.white,
              //   press: () {
              //     Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //         builder: (context) {
              //           return SignUpScreen();
              //         },
              //       ),
              //     );
              //   },
              // ),
              // Image(
              //   image: AssetImage("assets/images/4.png"),
              //   height: size.height * 1,
              //   alignment: Alignment.bottomCenter,
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
