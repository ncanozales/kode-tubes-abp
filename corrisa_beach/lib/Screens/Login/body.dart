import 'package:corrisa_beach/main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:corrisa_beach/Screens/Login/background.dart';
import 'package:corrisa_beach/Screens/Signup/signup_screen.dart';
import 'package:corrisa_beach/components/already_have_an_account_acheck.dart';
import 'package:corrisa_beach/components/rounded_button.dart';
import 'package:corrisa_beach/components/rounded_input_field.dart';
import 'package:corrisa_beach/components/rounded_password_field.dart';
import 'package:flutter_svg/svg.dart';

import '../../components/or_divider.dart';
import '../../components/social_icon.dart';
import '../Home/home.dart';

class Body extends StatefulWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final emailController = TextEditingController();
  final passController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage("assets/images/x.png"),
              height: size.height * 0.31,
            ),
            // Text(
            //   "LOGIN",
            //   style: TextStyle(fontWeight: FontWeight.bold),
            // ),
            // SizedBox(height: size.height * 0.03),
            // SvgPicture.asset(
            //   "assets/icons/login.svg",
            //   height: size.height * 0.35,
            // ),
            SizedBox(height: size.height * 0.02),
            RoundedInputField(
              color: Color.fromRGBO(37, 186, 255, 1),
              color2: Color.fromARGB(255, 202, 243, 255),
              hintText: "Email",
              controller: emailController,
            ),
            RoundedPasswordField(
              color: Color.fromRGBO(37, 186, 255, 1),
              color2: Color.fromARGB(255, 202, 243, 255),
              controller: passController,
            ),
            RoundedButton(
              color: Color.fromRGBO(37, 186, 255, 1),
              text: "LOGIN",
              press: () {
                logIn();
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) {
                //       return Home();
                //     },
                //   ),
                // );
              },
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              color: Color.fromRGBO(37, 186, 255, 1),
              press: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                )
              },
            ),
            OrDivider(
              color: Color.fromRGBO(37, 186, 255, 1),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocalIcon(
                  color: Color.fromARGB(255, 202, 243, 255),
                  iconSrc: "assets/icons/facebook.svg",
                  press: () {},
                ),
                SocalIcon(
                  color: Color.fromARGB(255, 202, 243, 255),
                  iconSrc: "assets/icons/google.svg",
                  press: () {},
                ),
                SocalIcon(
                  color: Color.fromARGB(255, 202, 243, 255),
                  iconSrc: "assets/icons/twitter2.svg",
                  press: () {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future logIn() async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => Center(
              child: CircularProgressIndicator(),
            ));

    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: emailController.text.trim(),
          password: passController.text.trim());
    } on FirebaseAuthException catch (e) {
      await showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) => AlertDialog(
                title: Text(
                  "Pengumuman",
                  style: TextStyle(color: Color.fromRGBO(37, 186, 255, 1)),
                ),
                content: Text(e.message!),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        "OK",
                        style:
                            TextStyle(color: Color.fromRGBO(37, 186, 255, 1)),
                      ))
                ],
              ));
      print(e);
    }

    navigatorKey.currentState!.popUntil((route) => route.isFirst);
  }
}
