import 'package:corrisa_beach/Screens/Home/my_colors.dart';
import 'package:corrisa_beach/Screens/Home/my_font_size.dart';
import 'package:corrisa_beach/Screens/Home/userpage.dart';
import 'package:corrisa_beach/constants.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';

class User {
  final String username;
  final String email;
  final String urlAvatar;
  final String status;

  const User(
      {required this.username,
      required this.email,
      required this.urlAvatar,
      required this.status});
}

class tab1 extends StatefulWidget {
  const tab1({Key? key}) : super(key: key);

  @override
  State<tab1> createState() => _tab1State();
}

class _tab1State extends State<tab1> {
  // Efek Notifikasi
  // List<bool> tabBarBadgeList = [
  //   false,
  //   false,
  //   false,
  //   true,
  // ];

  List tabBarList = [
    "Verified",
    "Pending",
    "Canceled",
    "All",
  ];

  int tabBarIndex = 0;
  int balanceBalance = 0;
  bool isBrush = false;
  bool isCollapseNavBottom = true;

  var _scrollController = ScrollController();

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels > 0) {
        isBrush = true;
        setState(() {});
      } else {
        isBrush = false;
      }
    });
    super.initState();
  }

  final fb = FirebaseDatabase.instance;

  @override
  Widget build(BuildContext context) {
    final ref = fb.ref().child('pesanan');
    int a;
    var l, g, k, str, start, end, startIndex, endIndex, daftarPesanan, tes;

    Widget tabBarItem(int index) {
      return Expanded(
        child: Stack(
          children: [
            Container(
                margin: EdgeInsets.all(5),
                height: double.infinity,
                decoration: BoxDecoration(
                    color: (tabBarIndex == index)
                        ? Colors.white // Warna Tiga
                        : Colors.transparent,
                    borderRadius: BorderRadius.circular(100)),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      tabBarIndex = index;
                    });
                  },
                  child: Center(
                    child: Text(
                      tabBarList[index],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: (tabBarIndex == index)
                              ? kPrimaryColor // Warna Empat
                              : MyColors.white,
                          fontSize: MyFontSize.medium1,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )),
            // Efek Notifikasi Pada Last Index
            // if (tabBarBadgeList[index])
            //   Align(
            //     alignment: Alignment.topRight,
            //     child: Container(
            //         height: 20,
            //         width: 20,
            //         decoration: BoxDecoration(
            //             color: MyColors.blue,
            //             borderRadius: BorderRadius.circular(100),
            //             border: Border.all(width: 1.5, color: MyColors.white)),
            //         child: Center(
            //           child: Container(
            //             height: 4,
            //             width: 4,
            //             decoration: BoxDecoration(
            //               color: MyColors.white,
            //               borderRadius: BorderRadius.circular(100),
            //             ),
            //           ),
            //         )),
            //   )
          ],
        ),
      );
    }

    Widget tabBar() {
      return Container(
        height: 40,
        width: double.infinity,
        decoration: BoxDecoration(
            color: kPrimaryColor, // Warna Dua
            borderRadius: BorderRadius.circular(100)),
        child: Row(
          children: [
            tabBarItem(0),
            tabBarItem(1),
            tabBarItem(2),
            tabBarItem(3),
          ],
        ),
      );
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white, // Warna Satu
          toolbarHeight: 70,
          title: tabBar(),
          elevation: 0.0,
          automaticallyImplyLeading: false,
        ),
        body: Container(
            // color: kPrimaryColor.withOpacity(.15),
            child: FirebaseAnimatedList(
          physics: BouncingScrollPhysics(),
          query: ref,
          shrinkWrap: true,
          itemBuilder: (context, snapshot, animation, index) {
            // print(snapshot.value);

            var v = snapshot.value
                .toString(); // {subtitle: webfun, title: subscribe}

            g = v.replaceAll(
                RegExp(
                    "{|}|uid: |verifikasi: |harga: |nama: |restoran: |tanggal: |jumlahTiket: |pesanan: "),
                ""); // webfun, subscribe
            g.trim();

            str = g;
            start = "[[";
            end = "]]";

            startIndex = str.indexOf(start);
            endIndex = str.indexOf(end, startIndex + start.length);
            String pesanan =
                "[" + str.substring(startIndex + start.length, endIndex) + "]";

            pesanan = pesanan.replaceAll(RegExp("], "), "],,");
            var daftarPesanan = pesanan.split(',,');

            String jumlahTiket;
            if (str.substring(str.length - 2).contains(" ")) {
              jumlahTiket = str.substring(str.length - 1);
            } else {
              jumlahTiket = str.substring(str.length - 2);
            }

            l = g.split(','); // [webfun,  subscribe]
            l[1] = l[1].replaceFirst(' ', '');

            String verifikasi = l[1].replaceFirst(' ', '');
            String harga = l[2].replaceFirst(' ', '');

            String nama = l[3].replaceFirst(' ', '');
            String restoran = l[4].replaceFirst(' ', '');
            String tanggal = l[5].replaceFirst(' ', '');

            if (tabBarIndex == 0) {
              if (verifikasi == "Yes") {
                return Card(
                  color: Colors.transparent,
                  elevation: 0.0,
                  child: ListTile(
                    onTap: () => {
                      setState(() {
                        k = snapshot.key;
                      }),
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => UserPage(
                                nama: nama,
                                k: k,
                                tiket: jumlahTiket,
                                harga: harga,
                                tanggal: tanggal,
                                verifikasi: verifikasi,
                                restoran: restoran,
                                daftarPesanan: daftarPesanan,
                              )))
                    },
                    title: Text(nama),
                    subtitle: Text(jumlahTiket + " Pesanan Tiket"),
                    // trailing: const Icon(Icons.arrow_forward),
                    trailing: Container(
                        padding: const EdgeInsets.fromLTRB(9, 9, 9, 9),
                        child: Text(
                          "VERIFIED",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'satu',
                            fontSize: 14,
                          ),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: kPrimaryColor)),
                    leading: CircleAvatar(
                      radius: 28,
                      backgroundImage: AssetImage("assets/images/dino1.jpg"),
                    ),
                  ),
                );
              } else {
                return SizedBox();
              }
            } else if (tabBarIndex == 1) {
              if (verifikasi == "No") {
                return Card(
                  color: Colors.transparent,
                  elevation: 0.0,
                  child: ListTile(
                    onTap: () => {
                      setState(() {
                        k = snapshot.key;
                      }),
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => UserPage(
                                nama: nama,
                                k: k,
                                tiket: jumlahTiket,
                                harga: harga,
                                tanggal: tanggal,
                                verifikasi: verifikasi,
                                restoran: restoran,
                                daftarPesanan: daftarPesanan,
                              )))
                    },
                    title: Text(nama),
                    subtitle: Text(jumlahTiket + " Pesanan Tiket"),
                    // trailing: const Icon(Icons.arrow_forward),
                    trailing: Container(
                        padding: const EdgeInsets.fromLTRB(9, 9, 9, 9),
                        child: Text(
                          "PENDING",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'satu',
                            fontSize: 14,
                          ),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: Colors.red)),
                    leading: CircleAvatar(
                      radius: 28,
                      backgroundImage: AssetImage("assets/images/dino1.jpg"),
                    ),
                  ),
                );
              } else {
                return SizedBox();
              }
            } else if (tabBarIndex == 2) {
              if (verifikasi == "Cancel") {
                return Card(
                  color: Colors.transparent,
                  elevation: 0.0,
                  child: ListTile(
                    onTap: () => {
                      setState(() {
                        k = snapshot.key;
                      }),
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => UserPage(
                                nama: nama,
                                k: k,
                                tiket: jumlahTiket,
                                harga: harga,
                                tanggal: tanggal,
                                verifikasi: verifikasi,
                                restoran: restoran,
                                daftarPesanan: daftarPesanan,
                              )))
                    },
                    title: Text(nama),
                    subtitle: Text(jumlahTiket + " Pesanan Tiket"),
                    // trailing: const Icon(Icons.arrow_forward),
                    trailing: Container(
                        padding: const EdgeInsets.fromLTRB(9, 9, 9, 9),
                        child: Text(
                          verifikasi,
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'satu',
                            fontSize: 14,
                          ),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: Colors.red)),
                    leading: CircleAvatar(
                      radius: 28,
                      backgroundImage: AssetImage("assets/images/dino1.jpg"),
                    ),
                  ),
                );
              } else {
                return SizedBox();
              }
            }
            return Card(
              color: Colors.transparent,
              elevation: 0.0,
              child: ListTile(
                onTap: () => {
                  setState(() {
                    k = snapshot.key;
                  }),
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => UserPage(
                            nama: nama,
                            k: k,
                            tiket: jumlahTiket,
                            harga: harga,
                            tanggal: tanggal,
                            verifikasi: verifikasi,
                            restoran: restoran,
                            daftarPesanan: daftarPesanan,
                          )))
                },
                title: Text(nama),
                subtitle: Text(jumlahTiket + " Pesanan Tiket"),
                // trailing: const Icon(Icons.arrow_forward),
                trailing: Container(
                    padding: const EdgeInsets.fromLTRB(9, 9, 9, 9),
                    child: Text(
                      verifikasi == "Yes" ? "VERIFIED" : "PENDING",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'satu',
                        fontSize: 14,
                      ),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color:
                            verifikasi == "Yes" ? kPrimaryColor : Colors.red)),
                leading: CircleAvatar(
                  radius: 28,
                  backgroundImage: AssetImage("assets/images/dino1.jpg"),
                ),
              ),
            );
          },
        )));
  }
}
