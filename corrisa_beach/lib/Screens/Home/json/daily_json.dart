const List daily = [
  {
    "icon": "assets/images/bank.png",
    "name": "Penjualan Tiket",
    "date": "85",
    "price": "Rp 3.550.000",
  },
  {
    "icon": "assets/images/auto.png",
    "name": "Jasa Pengantaran",
    "date": "Antar Jemput",
    "price": "Rp 1.500.000"
  },
  {
    "icon": "assets/images/gift.png",
    "name": "Pelayanan",
    "date": "Personal Service",
    "price": "Rp 2.225.000"
  },
  {
    "icon": "assets/images/eating.png",
    "name": "Restoran",
    "date": "Booking Fee",
    "price": "Rp 785.000"
  },
  {
    "icon": "assets/images/charity.png",
    "name": "Lain-lain",
    "date": "Tips",
    "price": "Rp 250.000"
  }
];

const List daily1 = [
  {
    "icon": "assets/images/bank.png",
    "name": "Penjualan Tiket",
    "date": "40",
    "price": "Rp 1.550.000"
  },
  {
    "icon": "assets/images/auto.png",
    "name": "Jasa Pengantaran",
    "date": "Antar Jemput",
    "price": "Rp 2.750.000"
  },
  {
    "icon": "assets/images/gift.png",
    "name": "Pelayanan",
    "date": "Personal Service",
    "price": "Rp 1.600.000"
  },
  {
    "icon": "assets/images/eating.png",
    "name": "Restoran",
    "date": "Booking Fee",
    "price": "Rp 900.000"
  },
  {
    "icon": "assets/images/charity.png",
    "name": "Lain-lain",
    "date": "Tips",
    "price": "Rp 550.000"
  }
];

const List daily2 = [
  {
    "icon": "assets/images/bank.png",
    "name": "Penjualan Tiket",
    "date": "168",
    "price": "Rp 15.550.000"
  },
  {
    "icon": "assets/images/auto.png",
    "name": "Jasa Pengantaran",
    "date": "Antar Jemput",
    "price": "Rp 7.500.000"
  },
  {
    "icon": "assets/images/gift.png",
    "name": "Pelayanan",
    "date": "Personal Service",
    "price": "Rp 9.225.000"
  },
  {
    "icon": "assets/images/eating.png",
    "name": "Restoran",
    "date": "Booking Fee",
    "price": "Rp 3.855.000"
  },
  {
    "icon": "assets/images/charity.png",
    "name": "Lain-lain",
    "date": "Tips",
    "price": "Rp 920.000"
  }
];

const List daily3 = [
  {
    "icon": "assets/images/bank.png",
    "name": "Penjualan Tiket",
    "date": "120",
    "price": "Rp 11.550.000"
  },
  {
    "icon": "assets/images/auto.png",
    "name": "Jasa Pengantaran",
    "date": "Antar Jemput",
    "price": "Rp 5.225.000"
  },
  {
    "icon": "assets/images/gift.png",
    "name": "Pelayanan",
    "date": "Personal Service",
    "price": "Rp 4.270.000"
  },
  {
    "icon": "assets/images/eating.png",
    "name": "Restoran",
    "date": "Booking Fee",
    "price": "Rp 985.000"
  },
  {
    "icon": "assets/images/charity.png",
    "name": "Lain-lain",
    "date": "Tips",
    "price": "Rp 620.000"
  }
];

const List daily4 = [
  {
    "icon": "assets/images/bank.png",
    "name": "Penjualan Tiket",
    "date": "10",
    "price": "Rp 550.000"
  },
  {
    "icon": "assets/images/auto.png",
    "name": "Jasa Pengantaran",
    "date": "Antar Jemput",
    "price": "Rp 400.000"
  },
  {
    "icon": "assets/images/gift.png",
    "name": "Pelayanan",
    "date": "Personal Service",
    "price": "Rp 885.000"
  },
  {
    "icon": "assets/images/eating.png",
    "name": "Restoran",
    "date": "Booking Fee",
    "price": "Rp 85.000"
  },
  {
    "icon": "assets/images/charity.png",
    "name": "Lain-lain",
    "date": "Tips",
    "price": "Rp 55.000"
  }
];

const List daily5 = [
  {
    "icon": "assets/images/bank.png",
    "name": "Penjualan Tiket",
    "date": "310",
    "price": "Rp 23.550.000"
  },
  {
    "icon": "assets/images/auto.png",
    "name": "Jasa Pengantaran",
    "date": "Antar Jemput",
    "price": "Rp 11.500.000"
  },
  {
    "icon": "assets/images/gift.png",
    "name": "Pelayanan",
    "date": "Personal Service",
    "price": "Rp 12.225.000"
  },
  {
    "icon": "assets/images/eating.png",
    "name": "Restoran",
    "date": "Booking Fee",
    "price": "Rp 1.785.000"
  },
  {
    "icon": "assets/images/charity.png",
    "name": "Lain-lain",
    "date": "Tips",
    "price": "Rp 1.250.000"
  }
];

const List daily6 = [
  {
    "icon": "assets/images/bank.png",
    "name": "Penjualan Tiket",
    "date": "473",
    "price": "Rp 43.550.000"
  },
  {
    "icon": "assets/images/auto.png",
    "name": "Jasa Pengantaran",
    "date": "Antar Jemput",
    "price": "Rp 21.800.000"
  },
  {
    "icon": "assets/images/gift.png",
    "name": "Pelayanan",
    "date": "Personal Service",
    "price": "Rp 72.525.000"
  },
  {
    "icon": "assets/images/eating.png",
    "name": "Restoran",
    "date": "Booking Fee",
    "price": "Rp 3.785.000"
  },
  {
    "icon": "assets/images/charity.png",
    "name": "Lain-lain",
    "date": "Tips",
    "price": "Rp 1.450.000"
  }
];
