import 'package:corrisa_beach/Screens/Home/tab1.dart';
import 'package:corrisa_beach/Screens/Home/tab2.dart';
import 'package:corrisa_beach/Screens/Home/tab4.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentIndex = 0;
  final screens = [tab1(), tab2(), ProfilePage()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // body: screens[currentIndex],
      body: IndexedStack(
        children: screens,
        index: currentIndex,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (index) => setState(() => currentIndex = index),
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        elevation: 0.0,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black.withOpacity(.4),
        showUnselectedLabels: false,
        showSelectedLabels: false,
        // selectedFontSize: 20,
        iconSize: 30,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.monitor_heart), label: 'Card'),
          BottomNavigationBarItem(
              icon: Icon(Icons.manage_accounts), label: 'Camera'),
        ],
      ),
    );
  }
}
