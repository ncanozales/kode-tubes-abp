import 'package:corrisa_beach/Screens/Home/home.dart';
import 'package:corrisa_beach/components/rounded_button.dart';
import 'package:corrisa_beach/constants.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class UserPage extends StatelessWidget {
  final String nama, k, tiket, tanggal, harga, restoran, verifikasi;
  final daftarPesanan;

  const UserPage(
      {Key? key,
      required this.nama,
      required this.k,
      required this.tiket,
      required this.tanggal,
      required this.restoran,
      required this.daftarPesanan,
      required this.verifikasi,
      required this.harga})
      : super(key: key);

  Container kartu(String title) {
    var tes;

    return Container(
      width: 300,
      child: Card(
        elevation: 0.0,
        child: ListTile(
          title: Text(
            title,
            style: TextStyle(
                fontFamily: 'lima',
                fontWeight: FontWeight.bold,
                color: kPrimaryColor),
          ),
          subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: new List.generate(daftarPesanan.length, (x) {
                tes = daftarPesanan[x].split(',');

                return Text(
                    tes[0].replaceFirst('[', '') +
                        " " +
                        tes[1].replaceFirst(' ', '') +
                        "x",
                    style: TextStyle(fontFamily: 'lima', color: kPrimaryColor));
              })),
          leading: const Icon(
            Icons.restaurant_menu,
            color: kPrimaryColor,
          ),
        ),
      ),
    );
  }

  Container kartu2(String title, Icon icon) {
    return Container(
      width: 350,
      child: Card(
        elevation: 0.0,
        child: ListTile(
          title: Text(
            title,
            style: TextStyle(
                fontFamily: 'enam',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: kPrimaryColor),
          ),
          leading: icon,
          // trailing: icon,
          iconColor: kPrimaryColor,
        ),
      ),
    );
  }

  Widget cekVerifikasi(context) {
    if (verifikasi == "Yes") {
      return RoundedButton(
          color: kPrimaryColor, text: "SUDAH TERVERIFIKASI", press: () {});
    } else {
      return RoundedButton(
          color: kPrimaryColor,
          text: "VERIFIKASI SEKARANG",
          press: () {
            upd();
            showDialog(
                barrierDismissible: false,
                context: context,
                builder: (context) => AlertDialog(
                      title: Text(
                        "Pengumuman",
                        style: TextStyle(color: kPrimaryColor),
                      ),
                      content: Text("Verification Success"),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Home()),
                                  (route) => false);
                            },
                            child: Text(
                              "OK",
                              style: TextStyle(color: kPrimaryColor),
                            ))
                      ],
                    ));
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        foregroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child: Center(
            child: Column(
          children: <Widget>[
            SizedBox(
              height: 5,
            ),
            CircleAvatar(
              backgroundImage: AssetImage("assets/images/dino1.jpg"),
              radius: 85,
            ),
            const SizedBox(height: 26),
            Text(
              nama,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'SATU',
                  color: Colors.black),
            ),
            const SizedBox(height: 10),
            kartu2(
                tiket + " TIKET",
                Icon(
                  Icons.calendar_view_day,
                  size: 30,
                )),
            Divider(
              color: kPrimaryColor,
            ),
            kartu2(
                tanggal,
                Icon(
                  Icons.calendar_month,
                  size: 30,
                )),
            Divider(
              color: kPrimaryColor,
            ),
            kartu2(
                restoran,
                Icon(
                  Icons.houseboat,
                  size: 30,
                )),
            kartu("Pesanan"),
            Divider(
              color: kPrimaryColor,
            ),
            kartu2(
                "Rp " + harga + "K",
                Icon(
                  Icons.monetization_on_rounded,
                  size: 30,
                )),
            cekVerifikasi(context),
            SizedBox(
              height: 10,
            )
          ],
        )),
      ),
    );
  }

  upd() async {
    DatabaseReference ref1 = FirebaseDatabase.instance.ref("pesanan/$k");

    await ref1.update({
      "verifikasi": "Yes",
    });
  }
}
