import 'package:flutter/material.dart';
import 'package:corrisa_beach/components/text_field_container.dart';
import 'package:corrisa_beach/constants.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final Color color, color2;
  final TextEditingController controller;

  const RoundedInputField({
    Key? key,
    this.hintText = '',
    required this.controller,
    required this.color,
    required this.color2,
    this.icon = Icons.person,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      color: this.color2,
      child: TextField(
        controller: controller,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: this.color,
          ),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}
