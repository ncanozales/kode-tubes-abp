import 'package:flutter/material.dart';
import 'package:corrisa_beach/components/text_field_container.dart';
import 'package:corrisa_beach/constants.dart';

class RoundedPasswordField extends StatelessWidget {
  final TextEditingController controller;
  final Color color, color2;
  const RoundedPasswordField(
      {Key? key,
      required this.color,
      required this.color2,
      required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      color: this.color2,
      child: TextField(
        obscureText: true,
        controller: controller,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          hintText: "Password",
          icon: Icon(
            Icons.lock,
            color: this.color,
          ),
          // suffixIcon: Icon(
          //   Icons.visibility,
          //   color: this.color,
          // ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
