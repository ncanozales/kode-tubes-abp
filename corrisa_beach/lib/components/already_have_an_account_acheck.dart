import 'package:flutter/material.dart';

class AlreadyHaveAnAccountCheck extends StatelessWidget {
  final bool login;
  final Function press;
  final Color color;
  const AlreadyHaveAnAccountCheck({
    Key? key,
    this.login = true,
    required this.press,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          login ? "Belum Memiliki Account ? " : "Sudah Memiliki Account ? ",
          style: TextStyle(color: this.color),
        ),
        GestureDetector(
          onTap: () => press(),
          child: Text(
            login ? "Register" : "Login",
            style: TextStyle(
              color: this.color,
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      ],
    );
  }
}
